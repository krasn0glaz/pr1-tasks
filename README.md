# Compilation of all PR1 tasks done by me

This is a compilation of all PR1 Homework Assignments solved by me.
Will be updated in realtime! Has WIP branches to it! Pull requests are welcome!

## Contents

__Content is sorted into semester folders__

For example:

### SS22
- Ex\* - Assignments by number
- scripts - my build scripts
- junk - junk
- praxis - tasks we are solving in class (on Tuesdays in the PC-Room)
