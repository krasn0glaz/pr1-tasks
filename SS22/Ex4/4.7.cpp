#include <iostream>

using std::cin, std::cout;

int binomial(int n, int k) {
    if (k == 0 || k == n)
        return 1;
    return binomial(n - 1, k - 1) + binomial(n - 1, k);
}

int main() {
    int a, b;
    cout << "Geben Sie Zahl n ein: ";
    cin >> a;
    cout << "Geben Sie Zahl k ein: ";
    cin >> b;
    cout << "Binomial Koeffizient n über k ist: " << binomial(a, b);
    return 0;
}
