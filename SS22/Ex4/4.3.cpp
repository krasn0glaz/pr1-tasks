#include <iostream>
#include <map>
#include <vector>

using namespace std;

int main() {
    // Initialize variables
    string input{""};
    while (!cin.eof()) {
        // Input DATA
        // Init a vector of chars which represents the input string
        cout << "Geben Sie der Text ein: ";
        getline(cin, input);
        vector<char> line(input.begin(), input.end());

        // Map to count occasions
        map<char, int> count;
        for (auto& i : line) {
            auto res = count.insert(pair<char, int>(i, 1));
            if (res.second == false)
                res.first->second++;
        }
        for (auto& i : count) {
            cout << '[' << i.first << ']' << "(" << i.second << ") ";
        }
        cout << endl;
    }
    return 0;
}
