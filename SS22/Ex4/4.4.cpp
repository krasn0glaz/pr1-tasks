#include <iostream>
#include <vector>

using namespace std;

int main() {
    string input{""};
    while (!cin.eof()) {
        cout << "Geben Sie der Text ein: ";
        getline(cin, input);
        sort(input.begin(), input.end());
        cout << "Sortiert Text: " << input << endl;
    }
    return 0;
}
