#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

// TODO: caesar cipher
// BUG: Big key values go out of bounds?

int main() {
    // Initialize variables
    string input{""};
    unsigned short int key{0};
    char ch;

    while (!cin.eof()) {
        // Input DATA
        cout << "Geben Sie der Text ein: ";
        getline(cin, input);
        // Init a vector of chars which represents the input string
        vector<char> msg(input.begin(), input.end());
        cout << "Geben Sie den Schlüssel ein: ";
        getline(cin, input);
        key = stoi(input);

        for (auto i = 0; i != static_cast<int>(msg.size()); ++i) {
            ch = msg.at(i);
            if (ch >= 'a' && ch <= 'z') {
                ch = ch + key;
                if (ch > 'z') {
                    ch = ch - 'z' + 'a' - 1;
                }
                // Set encoded char
                msg.at(i) = ch;
            } else if (ch >= 'A' && ch <= 'Z') {
                ch = ch + key;
                if (ch > 'Z') {
                    ch = ch - 'Z' + 'A' - 1;
                }
                msg.at(i) = ch;
            }
        }

        // Output
        cout << "Encrypted text:\n";
        for (char c : msg)
            cout << c;
        cout << endl;
        // Clear buffers
        cin.clear();
        cin.sync();
    }
    return 0;
}
