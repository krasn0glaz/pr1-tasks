#include <iostream>

using namespace std;

// TODO: Schreiben Sie ein Programm, das eine dreistellige Zahl einliest und mit
// Hilfe der Ziffernsumme überprüft, ob die eingegebene Zahl durch 3 teilbar
// ist.

int main() {
    int m{0}, t{0};
    cout << "Geben Sie natürliche dreistellige Zahl m ein: ";
    cin >> m;
    if (to_string(m).length() != 3) {
        cout << "Fehler! Zahl ist größer oder kleiner als erwartet!";
        return 127;
    }
    for (; m != 0; m /= 10) {
        t += m % 10;
    }
    if (t % 3 == 0)
        cout << "Zahl ist durch 3 teilbar!";
    else
        cout << "Zahl ist durch 3 nicht teilbar.";
    return 0;
}
