#include <iostream>

int main() {
    bool more{true};
    while (more) {
        std::cout << "Weiter? ";
        char answer;
        std::cin >> answer;
        more = answer == 'j';
    }
    return 0;
}
