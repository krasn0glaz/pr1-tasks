#include <iostream>
using namespace std;

// Dieses Programm ermittelt den durchschnittlichen Benzinverbrauch
// eines Fahrzeuges in l/100km
int main() {
    int km{0};
    cout << "Bitte geben Sie die gefahrenen km ein: ";
    cin >> km;

    int liter{0};
    cout << "Bitte geben Sie die verbrauchten Liter ein: ";
    cin >> liter;

    cout << "Verbrauch pro 100 km ist "
         << static_cast<double>(liter) * 100. / km << " L\n";
    return 0;
}
