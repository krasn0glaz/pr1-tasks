#include<iostream>
using namespace std;

int main()
{
  double y {0};
  int count {0};
  while (count < 10) {
    y = y+0.1;
    count = count+1;
  }
  cout << y << " ist ";
  if (y != 1) //Ungleichheitszeichen, Umkehrung von ==
    cout << "nicht ";
  cout << "gleich 1\n";

  return 0;
}
