#include <cmath>
#include <iostream>

using namespace std;

int main() {
    double a{0}, b{0};
    cout << "Geben Sie Kathet a in cm ein: ";
    cin >> a;
    cout << "Geben Sie Kathet b in cm ein: ";
    cin >> b;
    if (a == 0 || b == 0) {
        cout << "Fehler! a oder b können nicht 0 sein!\n";
        return 127;
    }
    cout << "Hypotenuse c ist " << sqrt(a * a + b * b) << " cm\n";
    return 0;
}
