#include <cmath>
#include <iostream>

using namespace std;

bool isPrime(int n) {
    if (n == 1 || n == 0)
        return false;
    int div{2}, stop{static_cast<int>(sqrt(n) + 0.5)};
    while (div < stop) {
        if (n % div == 0) {
            return false;
        }
        ++div;
    }
    cout << n << " Primzahl gefunden\n";
    return true;
}

int main() {
    int input{0}, min{0}, max{0}, counter{0};
    cin >> max;
    min = max;
    while (cin >> input) {
        if (isPrime(input)) {
            if (input > max) {
                max = input;
            }
            if (input < min) {
                min = input;
            }
        }
        ++counter;
    }
    cout << "\nEND\n\nMaxprimzahl: " << max << "\nMinprimzahl: " << min
         << "\nMahl while(): " << counter << "\n";
    return 0;
}
