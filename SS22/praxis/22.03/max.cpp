#include <cmath>
#include <iostream>

using namespace std;

int main() {
    int input{0}, max{0}, div{2};
    cin >> max;
    while (cin >> input) {
        if (input > max) {
            max = input;
        }
    }
    cout << max << "\n";
    return 0;
}
