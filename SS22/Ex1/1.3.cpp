#include<iostream>
#include<cmath>

using namespace std;


int main() {
    // Formula: S = (R^2)/2 * (Pi*adeg/180deg - sin(a))
    double R {0}, a {0}, S {0}; 
    cout << "Geben Sie den Radius des Kreises ein: ";
    cin >> R;
    cout << "Geben Sie den Winkel des Kreises in Grad ein: ";
    cin >> a;
    a = (a * M_PI) / 180; // convert to rad
    S = ((R*R) / 2) * (a - sin(a));
    cout << "Fläche: " << S << "\n";
    return 0;
}
