#include<iostream>

using namespace std;

int main() {
    double a {0}, b {0}; 
    cout << "Geben Sie die Länge des Rechtecks ein: ";
    cin >> a; 
    cout << "Geben Sie jetzt die Breite des Rechtecks ein: ";
    cin >> b;
    cout << "\nUmfang: " << (a + b) * 2 << "\nFläche: " << a * b << "\n";
    return 0;
}
