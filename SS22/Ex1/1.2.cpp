#include<iostream>

using namespace std;

int main() {
    unsigned int anzahl {0};
    double preis {0}, mwst {0};
    cout << "Geben Sie den Preis des Produkts ein: ";
    cin >> preis;
    cout << "Geben Sie der Anzahl ein: ";
    cin >> anzahl;
    cout << "Geben Sie MwSt ein: ";
    cin >> mwst;
    cout << "Gesamtpreis incl. MwSt: " << (preis * anzahl) + ((preis * anzahl) * mwst / 100) << "\n"; // hier zahle ich gesamtsumme plus prozentsumme von die ganze summe; als resultat habe ich gesamtpreis mit MwSt inkludiert
    return 0;
}
