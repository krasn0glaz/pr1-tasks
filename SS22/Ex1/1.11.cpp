#include<iostream>

using namespace std;

// ich nutze hier Array nur für Praxis. wahrscheinlich ist is kein schnellsten Methode, doch möchte ich es sochererweise schreiben

int main() {
    double res [5] {0, 0, 0, 0, 0};
    double avg {0};
    // can use sizeof to measure the length but for what?

    for(unsigned int i = 0; i < 5; ++i) {
	cout << "Geben Sie Wert #" << i + 1 << " ein: ";
	cin >> res[i];
    }
    
    for(unsigned int i = 0; i < 5; ++i) {
	avg += res[i];
    }
    avg /= 5;
    cout << "Mittelwert ist "<< avg << "\n";
    return 0;
}
