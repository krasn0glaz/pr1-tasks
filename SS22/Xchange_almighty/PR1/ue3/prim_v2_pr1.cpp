#include<cmath>
#include<iostream>
using namespace std;
int main()
{
  int n;
  cout << "Geben Sie bitte eine Zahl ein: ";
  cin >> n;

  for(int i {2}; i <= sqrt(n); ++i) { //wenn n=a*b, muss mindestens einer der Faktoren <= sqrt(n) sein
    if (n%i == 0) {
      cout << n << " ist keine Primzahl\n";
      return 0;
    }
  }

  if (n <= 1) cout << n << " ist keine Primzahl\n"; //Sonderfall 1 und Fehleingaben ausschließen
  else cout << n << " ist eine Primzahl\n";
  return 0;
}