#include<iostream>
using namespace std;
int main()
{
  int row {0};
  while (row < 5) {
    int column {0};
    while (column < 5) {
      cout << '*';
      ++column;
    }
    cout << '\n';
    ++row;
  }
  return 0;
}

