#include<cmath>
#include<iostream>
using namespace std;

bool is_prime(int n) 
{
  int i {2};
  int stop {static_cast<int>(sqrt(n))};
  while (i <= stop) { //wenn n=a*b, muss mindestens einer der Faktoren <= sqrt(n) sein
    if (n%i == 0)
      return false;
    ++i;
  }

  return n>1;
}

int main()
{
  bool found_prime {false};
  int maximum; //Initialisierung nicht noetig (im Allgemeinen auch nicht moeglich)
  int n;  

  while (cin >> n) 
    if (!found_prime) {
      if (is_prime(n)) {
        maximum = n;
        found_prime = true;
      }
    }
    else
      if (n > maximum && is_prime(n)) //Reihenfolge egal?
        maximum = n;

  if (found_prime)
    cout << "Die maximale Primzahl ist " << maximum << '\n';
  else
    cout << "Es wurde keine Primzahl gefunden!\n";
  return 0;
}

