#include<iostream>
#include<stdexcept>
#include"/home/Xchange/ue4/exp1_lib_pr1.h"
using namespace std;

int main() {
  try {
    while (cin)
      cout << expression() << '\n';
  }
  catch (runtime_error& e) {
    cerr << e.what() << '\n';
    return 1;
  }
  catch (...) {
    cerr << "exception \n";
    return 2;
  }
  return 0;
}
