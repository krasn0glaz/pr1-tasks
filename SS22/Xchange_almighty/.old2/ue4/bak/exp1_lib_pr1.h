enum class Ttype {plus, minus, mult, div, mod, open_parentheses, close_parentheses, number, result, quit};

struct Token {
  Ttype type;
  double value;
  Token();
  Token(Ttype);
  Token(Ttype,double);
};

class Token_stream {
  Token buffer;
  bool full {false};

public:
  Token get();
  void putback(Token);
};

double expression();
double term();
double primary();

extern Token_stream ts;

