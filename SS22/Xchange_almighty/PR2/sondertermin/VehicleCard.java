
import java.util.*;

public class VehicleCard implements Comparable<VehicleCard> {
	public enum Category {
		//values: 
		//PRICE_EUR, CYLINDER_CAPACITY_CM3, ENGINE_POWER_HP, ACCELERATION_SEC, VELOCITY_KMH, CONSUMPTION_L;
		// (”Preis”,1), (”Hubraum”,5), (”Leistung”,4), (”Beschleunigung”,3), (”Geschwindigkeit”,2), (”Verbrauch”,0)
		
		PRICE_EUR("Preis", 1), 
		CYLINDER_CAPACITY_CM3("Hubraum", 5), 
		ENGINE_POWER_HP("Leistung", 4), 
		ACCELERATION_SEC("Beschleunigung", 3), 
		VELOCITY_KMH("Geschwindigkeit", 2), 
		CONSUMPTION_L("Verbrauch", 0){
			@Override
			public int bonus(Double value) { 
				return (int) (this.getFactor() + value); // must be overriden for CONSUMPTION L. returns int(value + factor). 
			}
		}; 
		
		final private String categoryName; 
		final private int factor; 
		
		
		private Category(String categoryName, int factor) { /*throws IllegalArgumentException if categoryName null or empty or if factor less than 0 */
			if((categoryName == null) || (categoryName.equals("")) || (factor < 0)) {
				throw new IllegalArgumentException("Das geht so nicht!");
			}
			this.categoryName = categoryName;
			this.factor = factor;
		}
		
		// return int(factor times value)
		public int bonus(Double value) { 
			return (int) (this.factor * value); // must be overriden for CONSUMPTION L. returns int(value + factor). 
		}
		
		public int getFactor() {
			return this.factor;
		} 
		
		@Override
		public String toString() { 
			return this.categoryName;
		}
	} // Ende von enum Category

	
	
	private String name;
	private Map<Category, Double> categories;

	public VehicleCard(String name, Map<Category, Double> categories) {

		// throws IllegalArgumentException if name is null or empty.
		if((name == null) || (name.equals(""))) {
			throw new IllegalArgumentException("Name ist null oder leer.");
		}
		
		// throws IllegalArgumentException if categories is null not every Category exists in categories.
		if(categories == null) { throw new IllegalArgumentException("Map ist null"); }
		for(Category c: Category.values()) {
			if(!categories.containsKey(c)) {
				throw new IllegalArgumentException("Alle Kategorien müssen in der Map sein.");
			}
		}

		// throws IllegalArgumentException if categories contains any null value or values less than 0.
		for(Double value: categories.values()) {
			if((value == null) || (value < 0)) {
				throw new IllegalArgumentException("Map enthält Wert mit null oder unter 0.");
			}
		}
		
		// set member variables
		this.name = name;
		this.categories = categories;
	}

	
	// getters for immutable class, no setters (!)
	public String getName() {
		return name;
	}

	public Map<Category, Double> getCategories() {
		return categories;
	}
	

	
	// compare by totalBonus
	@Override 
	public int compareTo(VehicleCard other) {
		return this.totalBonus() - other.totalBonus();
	}

	
	// return Bonus value for category
	// Prof. im Video: Die Karte ist card. card.getNBonus Preis_EUR, dann soll aus dieser Map für Preis_Eur der 
	// Bonus berechnet werden, für diese eine Kategorie und zurückgegeben werden.
	// Also für Preis_Eur rufen Sie Bonus auf mit dem Value aus der Map von der card Karte.
	protected int getBonus(Category category) {
		return category.bonus(this.categories.get(category));
	}
	
	
	// returns total bonus of this card which is computed as the sum of the bonusValues (getBonus) of all the
	// categories assigned to this card.
	public int totalBonus() {
		int sum = 0;
		for(Category c: this.categories.keySet()) {
			sum += getBonus(c);					// Fehlt der Name der Karte?? beim Aufruf von bonus?
		}
		return sum;
	}
	

	// Soll beim Erstellen einer Card in der main helfen.
	// Im Video:
	// Prof.: Wenn Sie sich eine Map selbst erstellen wollen, das ist sehr mühsam. Deswegen gibt es diese Hilfsfunktion.
	// Die nimmt 6 Double-Werte entgegegen und soll eine Map returnieren. Dementsprechend können Sie dann schreiben: 
	// VehicleCard card = new VehicleCard name, VehicleCard.CategoriesMap und geben Sie die 6 Werte an.
	public static Map<Category, Double> newCategoriesMap(double price, double capa, double pwr, double acc, double velo, double cons){
		Map<Category, Double> neueMap = new TreeMap<Category, Double>();
		neueMap.put(Category.PRICE_EUR, price);
		neueMap.put(Category.CYLINDER_CAPACITY_CM3, capa);
		neueMap.put(Category.ENGINE_POWER_HP, pwr);
		neueMap.put(Category.ACCELERATION_SEC, acc);
		neueMap.put(Category.VELOCITY_KMH, velo);
		neueMap.put(Category.CONSUMPTION_L, cons);		
		return neueMap;
	}
	
	
	// returns category.toString()
	protected String categoryToString(Category category) { 
		return category.toString();
	}

	
	//− <name>(totalBonus) −> {<categories>}
	// use categoryToString for representation of Category values
	// Bsp: 
	// − Audi TT RS Roadster(73032) −> {Preis=58650.0, Hubraum=2480.0, Leistung=350.0, Beschleunigung
	// =4.6, Geschwindigkeit=280.0, Verbrauch=9.2}
	
	@Override
	public String toString() {
		String ausgabe = "- " + this.name + "(" + this.totalBonus() + ") -> {"; 
		int i = 0;
		for(Category c : this.categories.keySet()) {
			i++;
			ausgabe += categoryToString(c) + "=" + this.categories.get(c);
			if(i < this.categories.size()) {
				ausgabe += ", ";
			}
		}
		ausgabe += "}";
		return ausgabe;
	}
}