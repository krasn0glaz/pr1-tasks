
import java.util.*;

public class FoilVehicleCard extends VehicleCard {

	private List<Category> specials;

	public FoilVehicleCard(String name, Map<Category, Double> categories, List<Category> specials) {
		super(name, categories);

		// throws IllegalArgumentException if specials contains more than 3 items or is
		// null
		if (specials == null) {
			throw new IllegalArgumentException("specials darf nicht null sein.");
		}

		if (specials.size() > 3) {
			throw new IllegalArgumentException("specials darf höchstens drei Einträge haben.");
		}

		// set member variables
		this.specials = specials;
	}

	// returns twice the bonus value of the base class if category is a special
	// category,
	// and the bonus value of the base class otherwise
	@Override
	protected int getBonus(Category category) {
		int bonus = category.bonus(this.getCategories().get(category));
		if (this.specials.contains(category)) {
			return bonus * 2;
		} else {
			return bonus;
		}
	}

	// returns the same string as the base class, but prefixes and suffixes it with
	// an asterisk if category is a special category, e. g. ∗Preis∗
	@Override
	protected String categoryToString(Category category) {
		if (this.specials.contains(category)) {
			return "*" + category.toString() + "*";
		} else {
			return category.toString();
		}
	}

	
	public List<Category> getSpecials() {
		return this.specials;
	}
}
