
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

// Die zwei Aufrufe sind innerhalb der class Player, durch die vorgegeben Methoden
class PlayerBonusComparator implements Comparator<Player> {

	@Override
	public int compare(Player o1, Player o2) {
		return o1.totalBonus() - o2.totalBonus();
	}
}

class PlayerDeckSizeComparator implements Comparator<Player> {

	@Override
	public int compare(Player o1, Player o2) {
		return o1.getDeckSize() - o2.getDeckSize();
	}
}

public class Player implements Comparable<Player> {
	private String name;
	private Queue<VehicleCard> deck = new ArrayDeque<VehicleCard>();

	public Player(String name) {
		// throw IllegalArgumentException if name is null or empty
		if ((name == null) || (name.contentEquals(""))) {
			throw new IllegalArgumentException("Name darf nicht null oder leer sein.");
		}
		this.name = name;
	}

	public String getName() {
		return name;
	}

	// add cards to end
	public void addCards(Collection<VehicleCard> cards) {
		this.deck.addAll(cards);
	}

	// add card to end
	public void addCard(VehicleCard card) {
		this.deck.add(card);
	}

	public void clearDeck() {
		this.deck.clear();
	}

	// poll next card from deck
	public VehicleCard playNextCard() {
		return this.deck.poll();
	}

	// returns total bonus of this player which is computed as the sum of the
	// bonusValues (totalBonus) of all his cards
	public int totalBonus() {
		int sum = 0;
		for (VehicleCard c : this.deck) {
			sum += c.totalBonus();
		}
		return sum;
	}

	// compare by name[case insensitive]
	public int compareTo(Player other) {
		return this.name.compareToIgnoreCase(other.name);
	}

	// hash(name[case insensitive]
	// WO???
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	// auto generate but cmp name case insensitive
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equalsIgnoreCase(other.name))
			return false;
		return true;
	}

	public boolean challengePlayer(Player p) {
		// throws IllegalArgumentException if p is null or p is this.
		if ((p == null) || (this.equals(p))) {
			throw new IllegalArgumentException("Player ist null oder er selbst.");
		}

		List<VehicleCard> thisCards = new LinkedList<VehicleCard>();
		List<VehicleCard> psCards = new LinkedList<VehicleCard>();

		VehicleCard thisCardPlayed = null;
		VehicleCard psCardPlayed = null;
		while (!this.deck.isEmpty() && !p.deck.isEmpty()) {

			// playNextCard from this and p.

			thisCardPlayed = this.playNextCard();
			psCardPlayed = p.playNextCard();
			thisCards.add(thisCardPlayed);
			psCards.add(psCardPlayed);

			// Either scores are different (1) or draw (2)
			// If (1):
			// (a) Player who has higher scoring card, adds both of them to the end of his
			// deck. Order is not important.
			if ((thisCardPlayed.compareTo(psCardPlayed)) > 0) {
				this.addCards(thisCards);
				this.addCards(psCards);
				return true;
			}

			// (b) Player who has lower scoring card, loses card.
			if ((thisCardPlayed.compareTo(psCardPlayed)) < 0) {
				p.addCards(thisCards);
				p.addCards(psCards);
				return false;
			}

			// If (2), repeat until winner is found and
			// (a) Winner gets all cards played. Order is not important.
			// (b) Loser loses all cards played.
			// (c) If either deck is empty before winner is found, cards are returned to the
			// original deck.
			// Returns true if this wins. Else false.
		} // solange die Decks nicht leer sind wird Karte für Karte gezogen, bis mal zwei
			// unterschiedrliche Karte gezogen sind

		// Hat man keine zwei unterschiedlichen Karten gezogen und mindestens ein Deck
		// ist bereits leer,
		// Ausstieg aus der while
		this.addCards(thisCards); // Spieler erhalten ihre Spielkarten wieder retour
		p.addCards(psCards);
		return false; // Der Spieler verliert automatisch, weil eben nicht gewonnen
	}

	public static Comparator<Player> compareByBonus() {
		return new PlayerBonusComparator();
	}

	public static Comparator<Player> compareByDeckSize() {
		return new PlayerDeckSizeComparator();
	}

	// contains: Player.name (totalBonus), one card per line
	// Bsp:
	// Maria(73214):
	// − Porsche 911(73054) −> {Preis=<val> Hubraum=<val> ...}
	// − Renault Clio(160)−> {...}
	
	@Override
	public String toString() {
		String ret = this.name + "(" + this.totalBonus() + "):";
		for (VehicleCard c : this.deck) {
			ret += "\n" + c;
		}
		return ret;
	}

	public int getDeckSize() {
		return this.deck.size();
	}
}
