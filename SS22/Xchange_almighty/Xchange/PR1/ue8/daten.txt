﻿/*
Nicht alle der Strings sind gültige EAN_code. Array durchlaufen und für jeden einzelnen String
einen zugehörigen EAN_code-Code erzeugen. Falls nicht möglich -> entsprechende Fehlermeldung ausgeben
*/

vector<string> EAN_code_strings{"1234567890123","1111","12345678901234","9999999999999","123456789012X\n",
                   "123123123123","1234567890128","abcdefghijklm","1234567890129",
		   "1234567890120","1234567890121","1234567890122"};

/*
Ungültige Artikel (gültige kommen im Rahmen des Shops vor)
*/

Item item1{EAN_code{"123123123123"},
		"Bonbonniere",
		"Dieser Artikel sollte nicht erzeugt werden (ungueltiger EAN_code)",
		2540};
Item item2{EAN_code{"1234567890128"},
		"",
		"Dieser Artikel sollte nicht erzeugt werden (ungueltige Bezeichnung)",
		2540};
Item item3{EAN_code{"1234567890128"},
		"Bonbonniere",
		"Dieser Artikel sollte nicht erzeugt werden (ungueltiger Preis)",
		0};
Item item4{EAN_code{"1234567890128"},
		"Bonbonniere",
		"Dieser Artikel sollte nicht erzeugt werden (ungueltiger Preis, Compilerfehler)",
		11111111111111111111};


/*
Ungültige Shops können erzeugt werden, indem einfach (mindestens) einer der Strings auf 
leer gesetzt wird
*/

Shop the_shop{"https://buyhere.com","Der beste Shop","Rabattstrasse","80","4780","Billigsdorf",{
                Item{EAN_code{"1234567890128"},
		"Bonbonniere",
		"24 exquisite Schokoladestuecke + eine Ueberraschung",
		2540},
		Item{EAN_code{"1234567890129"},
		"Bonbonniere (einzeln)",
		"",
		99},
		Item{EAN_code{"1234567890120"},
		"Spezialmischung",
		"Limitierte Sonderauflage",
		4999},
		Item{EAN_code{"1234567890121"},
		"Spezialmischung (Doppelpackung)",
		"Unser Motto: Mehr essen und dabei gewaltig sparen!",
		9000},
		Item{EAN_code{"1234567890122"},
		"Spezialmischung (Familienpackung)",
		"Achtung: Fuer Haustiere ungeeignet!",
		11500},
		Item{EAN_code{"1234567890121"},
		"Unbekannter Artikel",
		"Dieser Artikel sollte nicht eingefuegt werden (EAN_code schon vorhanden)",
		10000}
             }};
