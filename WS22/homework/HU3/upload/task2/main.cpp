// Speech Numbers
// Author: Illia Bilyk

#include <iostream>
#include <stack>
#include <vector>

using std::cout, std::cin, std::vector, std::stack, std::string, std::to_string;

int main() {
    unsigned int n{0};
    vector<string> book{"null", "eins",  "zwei",   "drei", "vier",
                        "fünf", "sechs", "sieben", "acht", "neun"};
    stack<string> numOut;

    cout << "Geben Sie ein vierstellige Zahl ein: ";
    cin >> n;

    if (static_cast<int>(to_string(n).length()) != 4) {
        cout << "Fehler! Der Zahl ist kein vierstellige Integer Zahl!\n";
        return 127;
    }
    for (; n != 0; n /= 10)
        numOut.push(book.at(n % 10));
    while (!numOut.empty()) {
        cout << numOut.top() << "-";
        numOut.pop();
    }

    cout << '\b' << " " << '\n';
    return 0;
}
