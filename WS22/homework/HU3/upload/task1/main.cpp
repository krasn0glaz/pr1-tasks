// Sort 3 input double numbers without using logical operators
// Author: Illia Bilyk

#include <iostream>
#include <stdexcept>

using std::cout, std::cin, std::swap, std::runtime_error;

int main() {
    double a{0};
    double b{0};
    double c{0};
    cout << "Input a: ";
    if (!(cin >> a))
        throw runtime_error("Not a double!");
    cout << "Input b: ";
    cin >> b;
    cout << "Input c: ";
    cin >> c;
    if (a > c)
        swap(a, c);
    if (a > b)
        swap(a, b);
    if (b > c)
        swap(b, c);
    cout << "Sorted output:\nnew a: " << a << "\nnew b: " << b
         << "\nnew c: " << c << "\n";
    return 0;
}
