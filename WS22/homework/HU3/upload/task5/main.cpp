// Modulo Table Printer
// Author: Illia Bilyk

#include <iostream>
#include <vector>

using std::cout, std::cin, std::vector;

int main() {
    int n{0};

    cout << "Input modulo n: ";
    cin >> n;

    vector<vector<int>> table(n + 1, vector<int>(n + 1, 0));

    // debug out 1
    cout << "------ vector empty runthrough ------\n";
    for (vector<int> iv : table) {
        int c{0};
        int a{0};
        cout << c << " ";
        c++;
        for (int i : iv) {
            cout << a << " ";
            a++;
        }
        cout << "\n";
    }

    cout << "\n\n\n\n";
    cout << "------ vector WRITE ------\n";
    for (vector<int> iv : table) {
        int c{0};
        int a{0};

        cout << c << " ";
        c++;

        for (int i : iv) {
            cout << a << " ";
            a++;
        }

        cout << "\n";
    }

    return 0;
}
