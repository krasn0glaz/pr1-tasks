# task5

Create the addition table and the multiplication table modulo n. For example, the output for n=4 could look like:

Addition

0	 	1	 	2	 	3
1		2		3		0
2		3		0		1
3		0		1		2


Multiplication

0	 	0	 	0	 	0
0		1		2		3
0		2		0		2
0		3		2		1
