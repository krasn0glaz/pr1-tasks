// Square Equation Solver
// Author: Illia Bilyk

#include <cmath>
#include <iostream>

using std::cout, std::cin;

int main() {
    double p{0}, q{0}, D{0};
    cout << "Geben Sie die Koeffizienten p und q für Gleichung x²+px+q=0 ein: ";
    cin >> p >> q;
    if ((D = (p * p - 4 * q)) < 0) {
        cout << "\nGleichung hat keine Lösung! Diskriminant < 0!\n";
        return 0;
    } else if (D == 0)
        cout << "\nGleichung hat nur ein Wurzel!\nx = " << -p / 2 << "\n";
    else
        cout << "\nGleichung hat zwei Wurzeln!\nx1 = " << (-p + sqrt(D)) / 2
             << "\nx2 = " << (-p - sqrt(D)) / 2 << "\n";
    return 0;
}
