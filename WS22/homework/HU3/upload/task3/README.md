# task3

Write a program to solve the quadratic equation x²+px+q=0 (caution: watch out for possible complex solutions). Note: The cmath library contains the sqrt() function, which is used to calculate the square root. The passed value should not be less than 0 (zero), otherwise an error will be raised.
