// Speech Numbers
// Author: Illia Bilyk

#include <iostream>
#include <stack>
#include <vector>

using std::cout, std::cin, std::vector, std::stack, std::string, std::to_string;

int main() {
    unsigned int n{0};
    vector<string> book{
        "null",  "eins",   "zwei", "drei", "vier", "fünf",
        "sechs", "sieben", "acht", "neun", "punkt"}; // in order not to lose the
                                                     // dot on modding further I
                                                     // appended point as the
                                                     // last element
																										 // not sure whether it was a good idea
    stack<string> numOut;

    cout << "Geben Sie ein Zahl ein: ";
    cin >> n;

    // TODO: Implement a way to process input as a string to catch exceptions
    // 			 and dots in arbitrary numbers etc.

    for (; n != 0; n /= 10)
        numOut.push(book.at(n % 10));
    while (!numOut.empty()) {
        cout << numOut.top() << "-";
        numOut.pop();
    }

    cout << '\b' << " " << '\n';
    return 0;
}
