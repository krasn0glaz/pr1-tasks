# task15

Write a program that reads a four-digit integer and prints its linguistic representation.
E. g. input 1723 output one-seven-two-three.

(\*) Like task 2, but for arbitraty real numbers.
E. g.: input 17.23 output one-seven-point-two-three.
