#include <iostream>
    
int main() {
    unsigned int n {0};
    std::cout << "Geben sie ein Zahl ein: ";
    std::cin >> n;
    unsigned int summe {0};
    while(n > 0) {
	summe += n;
	--n; 
    }
    std::cout << "Summe aller natürlichen Zahlen vor n ist " << summe << ".\n";
return 0; 
}
